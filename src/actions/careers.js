export const FETCH_CAREERS_REQUESTED = ' FETCH_CAREERS_REQUESTED ';   
export const FETCH_CAREERS_SUCCEEDED = 'FETCH_CAREERS_SUCCEEDED'; 

export const fetchCareersRequested = () => ({type: FETCH_CAREERS_REQUESTED});
export const fetchCareersSucceeded = careers => ({type: FETCH_CAREERS_SUCCEEDED, careers});


export const FETCH_CAREER_REQUESTED = 'FETCH_CAREER_REQUESTED';
export const FETCH_CAREER_SUCCEEDED = 'FETCH_CAREER_SUCCEEDED'; 

export const fetchCareerRequested = id => ({type: FETCH_CAREER_REQUESTED, id});
export const fetchCareerSucceeded = career => ({type: FETCH_CAREER_SUCCEEDED, career});


export const UPDATE_CAREERS = 'UPDATE_CAREERS';
export const updateCareers = career =>({type: UPDATE_CAREERS, career});

export const SUBMIT_CAREER_REQUESTED = 'SUBMIT_CAREER_REQUESTED'; 
export const submitCareerRequested = () => ({type: SUBMIT_CAREER_REQUESTED});


export const SUBMIT_CAREER_SUCCEEDED = 'SUBMIT_CAREER_SUCCEEDED';
export const submitCareerSucceeded= (status, data) => ({type: SUBMIT_CAREER_SUCCEEDED, status, data});


export const DELETE_CAREER_REQUESTED = 'DELETE_CAREER_REQUESTED';
export const DELETE_CAREER_SUCCEEDED = 'DELETE_CAREER_SUCCEEDED';

export const deleteCareerRequested = id => ({type: DELETE_CAREER_REQUESTED, id});
export const deleteCareerSucceeded = ()  => ({type: DELETE_CAREER_SUCCEEDED});