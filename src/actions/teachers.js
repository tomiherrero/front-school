export const FETCH_TEACHERS_REQUESTED = ' FETCH_TEACHERS_REQUESTED ';   
export const FETCH_TEACHERS_SUCCEEDED = 'FETCH_TEACHERS_SUCCEEDED'; 

export const fetchTeachersRequested = () => ({type: FETCH_TEACHERS_REQUESTED});
export const fetchTeachersSucceeded = teachers => ({type: FETCH_TEACHERS_SUCCEEDED, teachers});

export const FETCH_TEACHER_REQUESTED = 'FETCH_TEACHER_REQUESTED';
export const FETCH_TEACHER_SUCCEEDED = 'FETCH_TEACHER_SUCCEEDED';

export const fetchTeacherRequested = id => ({type: FETCH_TEACHER_REQUESTED, id});
export const fetchTeacherSucceeded = teacher => ({type: FETCH_TEACHER_SUCCEEDED, teacher});

export const UPDATE_TEACHERS = 'UPDATE_TEACHERS';
export const updateTeachers= teacher =>({type: UPDATE_TEACHERS, teacher});

export const SUBMIT_TEACHER_REQUESTED = 'SUBMIT_TEACHER_REQUESTED'; 
export const submitTeacherRequested = () => ({type: SUBMIT_TEACHER_REQUESTED});

export const SUBMIT_TEACHER_SUCCEEDED = 'SUBMIT_TEACHER_SUCCEEDED';
export const submitTeacherSucceeded= (status, data) => ({type: SUBMIT_TEACHER_SUCCEEDED, status, data});

export const DELETE_TEACHER_REQUESTED = 'DELETE_TEACHER_REQUESTED';
export const DELETE_TEACHER_SUCCEEDED = 'DELETE_TEACHER_SUCCEEDED';

export const deleteTeacherRequested = id =>  ({type: DELETE_TEACHER_REQUESTED, id});
export const deleteTeacherSucceeded = ()  => ({type: DELETE_TEACHER_SUCCEEDED});
