export const FETCH_STUDENTNOTES_REQUESTED = 'FETCH_STUDENTNOTES_REQUESTED';
export const FETCH_STUDENTNOTES_SUCCEEDED = 'FETCH_STUDENTNOTES_SUCCEEDED';

export const fetchStudentNotesRequested = () => ({type: FETCH_STUDENTNOTES_REQUESTED});
export const fetchStudentNotesSucceeded = studentNotes => ({type: FETCH_STUDENTNOTES_SUCCEEDED, studentNotes})

export const FETCH_STUDENTNOTE_REQUESTED = 'FETCH_STUDENTNOTE_REQUESTED';
export const FETCH_STUDENTNOTE_SUCCEEDED = 'FETCH_STUDENTNOTE_SUCCEEDED';

export const fetchStudentNoteRequested = id => ({type: FETCH_STUDENTNOTE_REQUESTED, id});
export const fetchStudentNoteSucceeded = studentNote => ({type: FETCH_STUDENTNOTE_SUCCEEDED, studentNote});

export const UPDATE_STUDENTNOTES = 'UPDATE_STUDENTNOTES';
export const updateStudentNotes= studentNote =>({type: UPDATE_STUDENTNOTES , studentNote});

export const SUBMIT_STUDENTNOTE_REQUESTED = 'SUBMIT_STUDENTNOTE_REQUESTED'; 
export const submitStudentNoteRequested = () => ({type: SUBMIT_STUDENTNOTE_REQUESTED});

export const SUBMIT_STUDENTNOTE_SUCCEEDED = 'SUBMIT_STUDENTNOTE_SUCCEEDED';
export const submitStudentNoteSucceeded= (status, data) => ({type: SUBMIT_STUDENTNOTE_SUCCEEDED, status, data});

export const DELETE_STUDENTNOTE_REQUESTED = 'DELETE_STUDENTNOTE_REQUESTED';
export const DELETE_STUDENTNOTE_SUCCEEDED = 'DELETE_STUDENTNOTE_SUCCEEDED';

export const deleteStudentNoteRequested = id =>  ({type: DELETE_STUDENTNOTE_REQUESTED, id});
export const deleteStudentNoteSucceeded = ()  => ({type: DELETE_STUDENTNOTE_SUCCEEDED});

export const FETCH_COMBO_SUBJECTS_REQUESTED = 'FETCH_COMBO_SUBJECTS_REQUESTED';
export const FETCH_COMBO_SUBJECTS_SUCCEEDED = 'FETCH_COMBO_SUBJECTS_SUCCEEDED';

export const fetchComboSubjectsRequested = () => ({type: FETCH_COMBO_SUBJECTS_REQUESTED});
export const fetchComboSubjectsSucceeded = subjects => ({type: FETCH_COMBO_SUBJECTS_SUCCEEDED, subjects});

export const FETCH_COMBO_STUDENTS_REQUESTED = 'FETCH_COMBO_STUDENTS_REQUESTED';
export const FETCH_COMBO_STUDENTS_SUCCEEDED = 'FETCH_COMBO_STUDENTS_SUCCEEDED';

export const fetchComboStudentsRequested = () => ({type: FETCH_COMBO_STUDENTS_REQUESTED});
export const fetchComboStudentsSucceeded = students => ({type: FETCH_COMBO_STUDENTS_SUCCEEDED, students});

export const FETCH_COMBO_TEACHERS_REQUESTED = 'FETCH_COMBO_TEACHERS_REQUESTED';
export const FETCH_COMBO_TEACHERS_SUCCEEDED = 'FETCH_COMBO_TEACHERS_SUCCEEDED';

export const fetchComboTeachersRequested = () => ({type: FETCH_COMBO_TEACHERS_REQUESTED});
export const fetchComboTeachersSucceeded = teachers => ({type: FETCH_COMBO_TEACHERS_SUCCEEDED, teachers});