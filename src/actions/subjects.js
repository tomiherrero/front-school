export const FETCH_SUBJECTS_REQUESTED = 'FETCH_SUBJECTS_REQUESTED';
export const FETCH_SUBJECTS_SUCCEEDED = 'FETCH_SUBJECTS_SUCCEEDED';

export const fetchSubjectsRequested = () => ({type: FETCH_SUBJECTS_REQUESTED});
export const fetchSubjectsSucceeded = subjects => ({type: FETCH_SUBJECTS_SUCCEEDED, subjects});

export const FETCH_SUBJECT_REQUESTED = 'FETCH_SUBJECT_REQUESTED';
export const FETCH_SUBJECT_SUCCEEDED = 'FETCH_SUBJECT_SUCCEEDED';

export const fetchSubjectRequested = id => ({type: FETCH_SUBJECT_REQUESTED, id});
export const fetchSubjectSucceeded = subject => ({type: FETCH_SUBJECT_SUCCEEDED, subject});


export const UPDATE_SUBJECTS = 'UPDATE_SUBJECTS';
export const updateSubjects = subject => ({type: UPDATE_SUBJECTS, subject});

export const SUBMIT_SUBJECT_REQUESTED = 'SUBMIT_SUBJECT_REQUESTED';
export const submitSubjectRequested = () => ({type: SUBMIT_SUBJECT_REQUESTED});

export const SUBMIT_SUBJECT_SUCCEEDED = 'SUBMIT_SUBJECT_SUCCEEDED';
export const submitSubjectSucceeded = (status, data) => ({type: SUBMIT_SUBJECT_SUCCEEDED, status, data});

export const DELETE_SUBJECT_REQUESTED = 'DELETE_SUBJECT_REQUESTED'; 
export const DELETE_SUBJECT_SUCCEEDED = 'DELETE_SUBJECT_SUCCEEDED';

export const deleteSubjectRequested = id  => ({type: DELETE_SUBJECT_REQUESTED, id})
export const deleteSubjectSucceeded = () => ({type: DELETE_SUBJECT_SUCCEEDED})

