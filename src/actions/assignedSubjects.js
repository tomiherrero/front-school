
export const FETCH_ASSIGNEDSUBJECTS_REQUESTED = 'FETCH_ASSINGEDSUBJECTS_REQUESTED';
export const FETCH_ASSIGNEDSUBJECTS_SUCCEEDED = 'FETCH_ASSIGNEDSUBJECTS_SUCCEEDED';

export const fetchAssignedSubjectsRequested = () => ({type: FETCH_ASSIGNEDSUBJECTS_REQUESTED});
export const fetchAssignedSubjectsSucceeded = assignedSubjects => ({type: FETCH_ASSIGNEDSUBJECTS_SUCCEEDED, assignedSubjects});

export const FETCH_ASSIGNEDSUBJECT_REQUESTED = 'FETCH_ASSIGNEDSUBJECT_REQUESTED';
export const FETCH_ASSIGNEDSUBJECT_SUCCEEDED = 'FETCH_ASSIGNEDSUBJECT_SUCCEEDED';

export const fetchAssignedSubjectRequested = id => ({type: FETCH_ASSIGNEDSUBJECT_REQUESTED, id});
export const fetchAssignedSubjectSucceeded = assignedSubject => ({type: FETCH_ASSIGNEDSUBJECT_SUCCEEDED, assignedSubject});

export const UPDATE_ASSIGNEDSUBJECTS = 'UPDATE_ASSIGNEDSUBJECTS';
export const updateAssignedSubjects = assignedSubject => ({type: UPDATE_ASSIGNEDSUBJECTS, assignedSubject});

export const SUBMIT_ASSIGNEDSUBJECT_REQUESTED = 'SUBMIT_ASSIGNEDSUBJECT_REQUESTED';
export const submitAssignedSubjectRequested = () => ({type: SUBMIT_ASSIGNEDSUBJECT_REQUESTED});

export const SUBMIT_ASSIGNEDSUBJECT_SUCCEEDED = 'SUBMIT_ASSIGNEDSUBJECT_SUCCEEDED';
export const submitAssignedSubjectSucceeded = (status, data) => ({type: SUBMIT_ASSIGNEDSUBJECT_SUCCEEDED, status, data});

export const DELETE_ASSIGNEDSUBJECT_REQUESTED = 'DELETE_ASSIGNEDSUBJECT_REQUESTED';
export const DELETE_ASSIGNEDSUBJECT_SUCCEEDED = 'DELETE_ASSIGNEDSUBJECT_SUCCEEDED';

export const deleteAssignedSubjectRequested = id => ({type: DELETE_ASSIGNEDSUBJECT_REQUESTED, id});
export const deleteAssignedSubjectSucceeded = () => ({type: DELETE_ASSIGNEDSUBJECT_SUCCEEDED});

export const FETCH_COMBO_BOX_CAREERS_REQUESTED = 'FETCH_COMBO_BOX_CAREERS_REQUESTED';
export const FETCH_COMBO_BOX_CAREERS_SUCCEEDED = 'FETCH_COMBO_BOX_CAREERS_SUCCEEDED';

export const fetchComboBoxCareersRequested = () => ({type: FETCH_COMBO_BOX_CAREERS_REQUESTED});
export const fetchComboBoxCareersSucceeded = careers => ({type: FETCH_COMBO_BOX_CAREERS_SUCCEEDED, careers});

export const FETCH_COMBO_BOX_CAREER_REQUESTED = 'FETCH_COMBO_BOX_CAREER_REQUESTED';
export const FETCH_COMBO_BOX_CAREER_SUCCEEDED = 'FETCH_COMBO_BOX_CAREER_SUCCEEDED';

export const fetchComboBoxCareerRequested = id => ({type: FETCH_COMBO_BOX_CAREER_REQUESTED, id});
export const fetchComboBoxCareerSucceeded = career => ({type: FETCH_COMBO_BOX_CAREER_SUCCEEDED, career});

export const FETCH_COMBO_BOX_YEARS_REQUESTED = 'FETCH_COMBO_BOX_YEARS_REQUESTED';
export const FETCH_COMBO_BOX_YEARS_SUCCEEDED = 'FETCH_COMBO_BOX_YEARS_SUCCEEDED';

export const fetchComboBoxYearsRequested = () => ({type: FETCH_COMBO_BOX_YEARS_REQUESTED});
export const fetchComboBoxYearsSucceeded = years => ({type: FETCH_COMBO_BOX_YEARS_SUCCEEDED, years});

export const FETCH_COMBO_BOX_SUBJECTS_REQUESTED = 'FETCH_COMBO_BOX_SUBJECTS_REQUESTED';
export const FETCH_COMBO_BOX_SUBJECTS_SUCCEEDED = 'FETCH_COMBO_BOX_SUBJECTS_SUCCEEDED';

export const fetchComboBoxSubjectsRequested = () => ({type: FETCH_COMBO_BOX_SUBJECTS_REQUESTED});
export const fetchComboBoxSubjectsSucceeded = subjects => ({type: FETCH_COMBO_BOX_SUBJECTS_SUCCEEDED, subjects});

export const FETCH_COMBO_BOX_TEACHERS_REQUESTED = 'FETCH_COMBO_BOX_TEACHERS_REQUESTED';
export const FETCH_COMBO_BOX_TEACHERS_SUCCEEDED = 'FETCH_COMBO_BOX_TEACHERS_SUCCEEDED';

export const fetchComboBoxTeachersRequested = () => ({type: FETCH_COMBO_BOX_TEACHERS_REQUESTED});
export const fetchComboBoxTeachersSucceeded = teachers => ({type: FETCH_COMBO_BOX_TEACHERS_SUCCEEDED, teachers});

export const FETCH_COMBO_BOX_STUDENTS_REQUESTED = 'FETCH_COMBO_BOX_STUDENTS_REQUESTED';
export const FETCH_COMBO_BOX_STUDENTS_SUCCEEDED = 'FETCH_COMBO_BOX_STUDENTS_SUCCEEDED';

export const fetchComboBoxStudentsRequested = () => ({type: FETCH_COMBO_BOX_STUDENTS_REQUESTED});
export const fetchComboBoxStudentsSucceeded = students => ({type: FETCH_COMBO_BOX_STUDENTS_SUCCEEDED, students});
