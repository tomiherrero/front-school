export const FETCH_STUDENTS_REQUESTED = ' FETCH_STUDENTS_REQUESTED ';   
export const FETCH_STUDENTS_SUCCEEDED = 'FETCH_STUDENTS_SUCCEEDED'; 

export const fetchStudentsRequested = () => ({type: FETCH_STUDENTS_REQUESTED});
export const fetchStudentsSucceeded = students => ({type: FETCH_STUDENTS_SUCCEEDED, students});

export const FETCH_STUDENT_REQUESTED = 'FETCH_STUDENT_REQUESTED';
export const FETCH_STUDENT_SUCCEEDED = 'FETCH_STUDENT_SUCCEEDED';

export const fetchStudentRequested = id => ({type: FETCH_STUDENT_REQUESTED, id});
export const fetchStudentSucceeded = student => ({type: FETCH_STUDENT_SUCCEEDED, student});

export const UPDATE_STUDENTS = 'UPDATE_STUDENTS';
export const updateStudents = student =>({type: UPDATE_STUDENTS, student});

export const SUBMIT_STUDENT_REQUESTED = 'SUBMIT_STUDENT_REQUESTED'; 
export const submitStudentRequested = () => ({type: SUBMIT_STUDENT_REQUESTED});

export const SUBMIT_STUDENT_SUCCEEDED = 'SUBMIT_STUDENT_SUCCEEDED';
export const submitStudentSucceeded= (status, data) => ({type: SUBMIT_STUDENT_SUCCEEDED, status, data});

export const DELETE_STUDENT_REQUESTED = 'DELETE_STUDENT_REQUESTED';
export const DELETE_STUDENT_SUCCEEDED = 'DELETE_STUDENT_SUCCEEDED';

export const deleteStudentRequested = id =>  ({type: DELETE_STUDENT_REQUESTED, id});
export const deleteStudentSucceeded = ()  => ({type: DELETE_STUDENT_SUCCEEDED});


