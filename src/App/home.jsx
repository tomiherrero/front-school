import React from "react";
import { UncontrolledCarousel } from "reactstrap";
import customImage2 from "./picture/welcome.jpg";


const items = [
  {
    src: customImage2,
    key: "1",
  },
];

const Example = () => <UncontrolledCarousel items={items} />;

export default Example;
