import React, { Fragment } from "react";
import { HashRouter as Router, Switch, Route } from "react-router-dom";
import { Container, Row, Col } from "reactstrap";

import Header from "./Header";
import Footer from "./Footer";

import Home from './home';
import Careers from '../pages/Careers';
import Subjects from '../pages/Subjects';
import Teachers from '../pages/Teachers';
import Students from '../pages/Students';
import AssignedSubjects from '../pages/AssignedSubjects';
import StudentNotes from '../pages/StudentNotes';


const Index = () => (
  <Fragment>
    <Router>
      <Container>
        <Row>
          <Col>
            <Header />
            <main>
              <Switch>
                   <Route path = '/studentNotes' component = {StudentNotes}/>
                    <Route path ='/assignedSubjects' component = {AssignedSubjects} />
                    <Route path = '/students' component = {Students} />
                    <Route path = '/careers' component = {Careers} />
                    <Route path = '/subjects' component = {Subjects} />
                    <Route path = '/teachers' component = {Teachers} />
                    <Route path = '/' component = {Home} />
              </Switch>
            </main>
            <br />
            <Footer />
          </Col>
        </Row>
      </Container>
    </Router>
  </Fragment>
);
export default Index;
