import React, { useState } from "react";
import { Link } from "react-router-dom";


import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
} from "reactstrap";

const Header = () => {
    const [isOpen, setIsOpen] = useState(false);
  
    const toggle = () => setIsOpen(!isOpen);
  
    return (
      <header>
        <Navbar className= "navbar fixed-top navbar-expand-lg navbar-light bg-info" >
          <NavbarBrand tag={Link} to="/">
            Home
          </NavbarBrand>
          <NavbarToggler onClick={toggle} />
          <Collapse isOpen={isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink tag={Link} to="/careers">
                Carreras
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink tag={Link} to="/subjects">
                  Materias
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink tag={Link} to="/teachers">
                  Profesores
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink tag={Link} to="/students">
                Estudiantes
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink tag={Link} to="/assignedSubjects">
                Materias asignadas 
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink tag={Link} to="/studentNotes">
                  Notas de estudiantes
                </NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
        <br />
        <br />
      </header>
    );
  };
  
  export default Header;