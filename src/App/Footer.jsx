import React from "react";
import { Card, CardText } from "reactstrap";

const footer = (props) => {
  return (
    <footer>
      <Card
        body
        inverse
        style={{ backgroundColor: "#333", borderColor: "#555" }}
      >
        <br />
        <h1>Información del sitio web</h1>
        <CardText>Developers del INDEC.</CardText>
        <CardText> Tomas Agustin Herrero.</CardText>
        <CardText>Pablo Javier Navarro.</CardText>{" "}
      </Card>
    </footer>
  );
};

export default footer;
