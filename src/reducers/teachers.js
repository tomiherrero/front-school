import {
    FETCH_TEACHERS_REQUESTED,
    FETCH_TEACHERS_SUCCEEDED,
    UPDATE_TEACHERS,
    FETCH_TEACHER_REQUESTED,
    FETCH_TEACHER_SUCCEEDED,
  } from '../actions/teachers';
    
  const initialState = {
      teachers: [],
      currentTeachers: {
        name: "",
        surmane: "",
        phone: "",
        home:"",
        email: ""

      },
      headers: [ {
          label: 'Nombre',
          key: 'name',
        },
        {
        label: 'Apellido',
        key: 'surname',
        },
        {
        label: 'Telefono',
        key: 'phone',
        },
        {
         label: 'Dirección',
        key: 'home',
        },
        {
         label: 'Email',
         key: 'email',
        },
      ],
  };
  export default (state = initialState,  action) => {
      switch(action.type){
        case FETCH_TEACHERS_REQUESTED:
          return {...state, teachers:[]}
        case FETCH_TEACHERS_SUCCEEDED:
            return {...state, teachers: action.teachers}
        case UPDATE_TEACHERS:
              return {...state, currentTeachers: action.teacher}
        case FETCH_TEACHER_REQUESTED:
          return{...state, currentTeachers: initialState.currentTeachers}
        case FETCH_TEACHER_SUCCEEDED:
          return {...state, currentTeachers: action.teacher}    
        default:
            return {...state};
      }
  }