import { FETCH_ASSIGNEDSUBJECTS_REQUESTED, 
            FETCH_ASSIGNEDSUBJECTS_SUCCEEDED,
            UPDATE_ASSIGNEDSUBJECTS,
            FETCH_ASSIGNEDSUBJECT_REQUESTED,
            FETCH_ASSIGNEDSUBJECT_SUCCEEDED,
            FETCH_COMBO_BOX_CAREERS_REQUESTED,
            FETCH_COMBO_BOX_CAREERS_SUCCEEDED,
            FETCH_COMBO_BOX_YEARS_REQUESTED,
            FETCH_COMBO_BOX_YEARS_SUCCEEDED,
            FETCH_COMBO_BOX_SUBJECTS_REQUESTED,
            FETCH_COMBO_BOX_SUBJECTS_SUCCEEDED,
            FETCH_COMBO_BOX_TEACHERS_REQUESTED,
            FETCH_COMBO_BOX_TEACHERS_SUCCEEDED,
            FETCH_COMBO_BOX_STUDENTS_REQUESTED,
            FETCH_COMBO_BOX_STUDENTS_SUCCEEDED
        } from '../actions/assignedSubjects';

const initialState = {
    assignedSubjects: [], 
    currentAssignedSubjects: {
        careers: "", 
        years: "",
        subjects: "",
        teachers: "", 
        students: ""
       
    },
    headers: [{
        label: 'Carreras ',
        key: 'careers'
    },
    {
        label: 'Años ',
        key: 'years'
    },
    {
        label: 'Materias ',
        key: 'subjects'
    },
    {
        label: 'Profesores ',
        key: 'teachers'
    },
    {
        label: 'Alumnos ',
        key: 'students'
    }]
};
export default (state = initialState, action) => {
    switch(action.type){
        case FETCH_ASSIGNEDSUBJECTS_REQUESTED:
            return{...state, assignedSubjects: []}
        case FETCH_ASSIGNEDSUBJECTS_SUCCEEDED:
            return {...state, assignedSubjects: action.assignedSubjects}
        case UPDATE_ASSIGNEDSUBJECTS:
            return {...state, currentAssignedSubjects: action.assignedSubject}
        case FETCH_ASSIGNEDSUBJECT_REQUESTED:
            return {...state, currentAssignedSubjects: initialState.currentAssignedSubjects}
        case FETCH_ASSIGNEDSUBJECT_SUCCEEDED:
            return {...state, currentAssignedSubjects: action.assignedSubject}
        case FETCH_COMBO_BOX_CAREERS_REQUESTED:
            return {...state, careers: []}
        case FETCH_COMBO_BOX_CAREERS_SUCCEEDED:
            return{...state, careers: action.careers}
        case FETCH_COMBO_BOX_YEARS_REQUESTED:
            return {...state, years: []}
        case FETCH_COMBO_BOX_YEARS_SUCCEEDED:
            return {...state, years: action.years}
        case FETCH_COMBO_BOX_SUBJECTS_REQUESTED:
            return{...state, subjects: []}
        case FETCH_COMBO_BOX_SUBJECTS_SUCCEEDED: 
            return{...state, subjects: action.subjects}
        case FETCH_COMBO_BOX_TEACHERS_REQUESTED:
            return {...state, teachers: []}
        case FETCH_COMBO_BOX_TEACHERS_SUCCEEDED:
            return{...state, teachers: action.teachers}
        case FETCH_COMBO_BOX_STUDENTS_REQUESTED:
            return {...state, students: []}
        case FETCH_COMBO_BOX_STUDENTS_SUCCEEDED:
            return{...state, students: action.students}
        default:
            return{...state}
    }
}