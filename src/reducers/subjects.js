import {FETCH_SUBJECTS_REQUESTED, 
            FETCH_SUBJECTS_SUCCEEDED,
            UPDATE_SUBJECTS,
            FETCH_SUBJECT_REQUESTED,
            FETCH_SUBJECT_SUCCEEDED
} from '../actions/subjects';


const initialState = {
        subjects: [], 
        currentSubjects: {
            description: "",
            id: null
        },
        headers: [
            {
            label: 'Materias',
            key: 'description',
        },
    ],
};

export default (state = initialState, action) => {
    switch(action.type){
        case FETCH_SUBJECTS_REQUESTED:
            return {...state, subjects: []}
        case FETCH_SUBJECTS_SUCCEEDED:
            return {...state, subjects: action.subjects}
        case UPDATE_SUBJECTS:
            return {...state, currentSubjects: action.subject}
        case FETCH_SUBJECT_REQUESTED:
            return {...state, currentSubjects: initialState.currentSubjects}
        case FETCH_SUBJECT_SUCCEEDED:
            return {...state, currentSubjects: action.subject}
        default:
            return {...state}
    }
}

