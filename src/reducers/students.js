import {
    FETCH_STUDENTS_REQUESTED,
    FETCH_STUDENTS_SUCCEEDED,
    UPDATE_STUDENTS,
    FETCH_STUDENT_REQUESTED,
    FETCH_STUDENT_SUCCEEDED
  } from '../actions/students';
  
  const initialState = {
      students: [],
      currentStudents: {
        name: "",
        surname: "",
        phone: "",
        home: "",
        email: ""
    
      },
      headers: [ {
          label: 'Nombre',
          key: 'name',
        },
        {
            label: 'Apellido',
            key: 'surname',
          },
          {
            label: 'Telefono',
            key: 'phone',
          },
          {
            label: 'Direccion',
            key: 'home',
          },
          {
            label: 'Email',
            key: 'email',
          },
      ],
  };
  export default (state = initialState,  action) => {
      switch(action.type){
        case FETCH_STUDENTS_REQUESTED:
          return {...state, students:[]}
        case FETCH_STUDENTS_SUCCEEDED:
            return {...state, students: action.students}
        case UPDATE_STUDENTS:
          return {...state, currentStudents: action.student}
          case FETCH_STUDENT_REQUESTED:
          return{...state, currentStudents: initialState.currentStudents}
        case FETCH_STUDENT_SUCCEEDED:
          return {...state, currentStudents: action.student} 
        default:
            return {...state};
      }
  }