import { combineReducers } from "redux";
import careers from "./careers";
import subjects from './subjects';
import teachers from "./teachers";
import students from "./students";
import assignedSubjects from "./assignedSubjects";
import studentNotes from "./studentNotes";

export default combineReducers ({careers, subjects, teachers, students, assignedSubjects, studentNotes})



