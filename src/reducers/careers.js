import {
    FETCH_CAREERS_REQUESTED,
    FETCH_CAREERS_SUCCEEDED,
    UPDATE_CAREERS,
    FETCH_CAREER_REQUESTED,
    FETCH_CAREER_SUCCEEDED,
  } from '../actions/careers';
  
  
  const initialState = {
      careers: [],
      currentCareers: {
        description: "",
        id: null
      },
      headers: [ {
          label: 'Carreras',
          key: 'description',
        },
      ],
  };
  export default (state = initialState,  action) => {
      switch(action.type){
        case FETCH_CAREERS_REQUESTED:
          return {...state, careers:[]}
        case FETCH_CAREERS_SUCCEEDED:
            return {...state, careers: action.careers}
        case UPDATE_CAREERS:
            return {...state, currentCareers: action.career}
        case FETCH_CAREER_REQUESTED:
            return {...state, currentCareers: initialState.currentCareers}
        case FETCH_CAREER_SUCCEEDED: 
            return {...state, currentCareers: action.career}
        default:
            return {...state};
      }
  }