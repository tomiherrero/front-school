import {
    FETCH_STUDENTNOTES_REQUESTED,
    FETCH_STUDENTNOTES_SUCCEEDED,
    UPDATE_STUDENTNOTES,
    FETCH_STUDENTNOTE_REQUESTED,
    FETCH_STUDENTNOTE_SUCCEEDED,
    FETCH_COMBO_SUBJECTS_REQUESTED,
    FETCH_COMBO_SUBJECTS_SUCCEEDED,
    FETCH_COMBO_STUDENTS_REQUESTED,
    FETCH_COMBO_STUDENTS_SUCCEEDED,
    FETCH_COMBO_TEACHERS_REQUESTED,
    FETCH_COMBO_TEACHERS_SUCCEEDED
} from '../actions/studentNotes';

const initialState = {
    studentNotes: [],
    currentStudentNotes: {
        note1: "",
        note2: "",
        note3: "",
        averages: "",
        subjects: "",
        students: "",
        teachers: ""
    
    },
    headers: [ {
        label: 'Nota 1',
        key: 'note1',
    },
    {
        label: 'Nota 2',
        key: 'note2',
    },
    {
        label: 'Nota 3',
        key: 'note3',
    },
    {
        label: 'Promedio',
        key: 'averages',
    },
    {
        label: 'Materia',
        key: 'subjects',
    },
    {
        label: 'Estudiante',
        key: 'students',
    },
    {
        label: 'Profesor',
        key: 'teachers',
    },
  ],
};
export default(state = initialState, action) => {
    switch (action.type) {
        case FETCH_STUDENTNOTES_REQUESTED:
            return{...state, studentNotes:[]}
        case FETCH_STUDENTNOTES_SUCCEEDED:
            return{...state, studentNotes: action.studentNotes}
        case UPDATE_STUDENTNOTES:
          return {...state, currentStudentNotes: action.studentNote}
        case FETCH_STUDENTNOTE_REQUESTED:
            return{...state, currentStudentNotes: initialState.currentStudentNotes}
        case FETCH_STUDENTNOTE_SUCCEEDED:
            return {...state, currentStudentNotes: action.studentNote} 
        case FETCH_COMBO_SUBJECTS_REQUESTED:
            return{...state, subjects: []}
        case FETCH_COMBO_SUBJECTS_SUCCEEDED:
            return{...state, subjects: action.subjects}   
        case FETCH_COMBO_STUDENTS_REQUESTED:
            return{...state, students: []}
        case FETCH_COMBO_STUDENTS_SUCCEEDED:
            return{...state, students: action.students}        
       case FETCH_COMBO_TEACHERS_REQUESTED:
            return{...state, teachers: []}
        case FETCH_COMBO_TEACHERS_SUCCEEDED:
            return{...state, teachers: action.teachers}                  
        default:
            return{...state};       
    }
}