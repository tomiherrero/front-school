import {put, call, select} from 'redux-saga/effects';
import AssignedSubjectService from '../services/assignedSubjects';
import { fetchAssignedSubjectsSucceeded, 
            fetchAssignedSubjectSucceeded, 
            deleteAssignedSubjectSucceeded, 
            fetchComboBoxCareersSucceeded, 
            submitAssignedSubjectSucceeded,        
            fetchComboBoxYearsSucceeded,
            fetchComboBoxSubjectsSucceeded,
            fetchComboBoxTeachersSucceeded,
            fetchComboBoxStudentsSucceeded} from '../actions/assignedSubjects';



export function* fetchAssignedSubjects ({filter}) {
    const assignedSubjects = yield call (AssignedSubjectService.apiCall, filter);
    yield put(fetchAssignedSubjectsSucceeded(assignedSubjects))
}

export function* fetchComboBoxCareers (){
    const careers = yield call(AssignedSubjectService.apiCallCareers);
   yield put(fetchComboBoxCareersSucceeded(careers))
}

export function* fetchComboBoxYears(){
    const years = yield call(AssignedSubjectService.apiCallYears)
    yield put(fetchComboBoxYearsSucceeded(years))
}
export function* fetchComboBoxSubjects(){
    const subjects = yield call(AssignedSubjectService.apiCallSubjects);
    yield put(fetchComboBoxSubjectsSucceeded(subjects))
}

export function* fetchComboBoxTeachers() {
    const teachers = yield call(AssignedSubjectService.apiCallTeachers)
    yield put(fetchComboBoxTeachersSucceeded(teachers))
}

export function* fetchComboBoxStudents() {
    const students = yield call(AssignedSubjectService.apiCallStudents)
    yield put(fetchComboBoxStudentsSucceeded(students))
}
export function* submitAssignedSubject(){
    const {currentAssignedSubjects} = yield select (state => state.assignedSubjects);
    const {status, data} = yield call(AssignedSubjectService.apiSave, currentAssignedSubjects);
    yield put(submitAssignedSubjectSucceeded(status, data))
}

export function* fetchAssignedSubject({id}){
    const assignedSubject = yield call(AssignedSubjectService.apiCallOne, id)
    yield put(fetchAssignedSubjectSucceeded(assignedSubject));
}

export function* deleteAssignedSubject({id}){
    yield call(AssignedSubjectService.apiDelete, id)
    yield put(deleteAssignedSubjectSucceeded(true))
    yield call(fetchAssignedSubjects, {})
}