import {call, put, select} from 'redux-saga/effects';
import StudentService from '../services/students';
import {fetchStudentsSucceeded, submitStudentSucceeded, fetchStudentSucceeded, 
              deleteStudentSucceeded} from '../actions/students';

export function* fetchStudents({filter}) {
    const students = yield call(StudentService.apiCall, filter)
    yield put(fetchStudentsSucceeded(students));
}

export function* submitStudent() {
    const {currentStudents} = yield select (state => state.students);
    const {status, data} = yield call(StudentService.apiSave, currentStudents);
    yield put(submitStudentSucceeded(status, data));
  }

  export function * fetchStudent ({id}){
    const student = yield call (StudentService.apiCallOne, id)
    yield put(fetchStudentSucceeded(student));
}

export function* deleteStudent({id}){
  yield call(StudentService.apiDelete,id);
  yield put(deleteStudentSucceeded(true));
  yield call(fetchStudents, {});
}

