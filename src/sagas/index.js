import {all, takeEvery} from 'redux-saga/effects'

import {FETCH_CAREERS_REQUESTED, 
        SUBMIT_CAREER_REQUESTED, 
        FETCH_CAREER_REQUESTED, 
        DELETE_CAREER_REQUESTED} from '../actions/careers'
        
import {  fetchCareers,
                submitCareer, 
                fetchCareer,
                deleteCareer} from './careers'

import  { FETCH_STUDENTS_REQUESTED,
                SUBMIT_STUDENT_REQUESTED, 
                FETCH_STUDENT_REQUESTED,
                DELETE_STUDENT_REQUESTED} from '../actions/students';

import {   fetchStudents, 
                submitStudent, 
                fetchStudent,
                 deleteStudent} from './students'

import {FETCH_SUBJECTS_REQUESTED,
        SUBMIT_SUBJECT_REQUESTED, 
        FETCH_SUBJECT_REQUESTED,
        DELETE_SUBJECT_REQUESTED} from '../actions/subjects'
        
import {fetchSubjects,
        submitSubject,
        fetchSubject,
        deleteSubject } from './subjects'

import {FETCH_TEACHERS_REQUESTED,
        SUBMIT_TEACHER_REQUESTED,
        FETCH_TEACHER_REQUESTED,
        DELETE_TEACHER_REQUESTED}from '../actions/teachers'

import {fetchTeachers,
        submitTeacher,
        fetchTeacher,
        deleteTeacher} from './teachers'

import {FETCH_ASSIGNEDSUBJECTS_REQUESTED,
        SUBMIT_ASSIGNEDSUBJECT_REQUESTED, 
        FETCH_ASSIGNEDSUBJECT_REQUESTED, 
        DELETE_ASSIGNEDSUBJECT_REQUESTED,
        FETCH_COMBO_BOX_CAREERS_REQUESTED, 
        FETCH_COMBO_BOX_YEARS_REQUESTED, 
        FETCH_COMBO_BOX_SUBJECTS_REQUESTED, 
        FETCH_COMBO_BOX_TEACHERS_REQUESTED,
        FETCH_COMBO_BOX_STUDENTS_REQUESTED } from '../actions/assignedSubjects'
            
import {fetchAssignedSubjects,
        submitAssignedSubject,
        fetchAssignedSubject,
        deleteAssignedSubject,
        fetchComboBoxCareers,
        fetchComboBoxYears,
        fetchComboBoxSubjects, 
        fetchComboBoxTeachers,
        fetchComboBoxStudents} from './assignedSubjects'
            
import{ FETCH_STUDENTNOTES_REQUESTED, 
        SUBMIT_STUDENTNOTE_REQUESTED,
        FETCH_STUDENTNOTE_REQUESTED,
        DELETE_STUDENTNOTE_REQUESTED,
        FETCH_COMBO_SUBJECTS_REQUESTED, 
        FETCH_COMBO_STUDENTS_REQUESTED,
        FETCH_COMBO_TEACHERS_REQUESTED} from '../actions/studentNotes';
            
import{ fetchStudentNotes,
        submitStudentNote,
        fetchStudentNote,
        deleteStudentNote,
        fetchComboSubjects, 
        fetchComboStudents, 
        fetchComboTeachers} from './studentNotes'


export default function*  root () {
    yield  all ([
        takeEvery(FETCH_CAREERS_REQUESTED, fetchCareers),
        takeEvery(SUBMIT_CAREER_REQUESTED, submitCareer),
        takeEvery(FETCH_CAREER_REQUESTED, fetchCareer),
        takeEvery(DELETE_CAREER_REQUESTED, deleteCareer),

        takeEvery(FETCH_SUBJECTS_REQUESTED, fetchSubjects),
        takeEvery(SUBMIT_SUBJECT_REQUESTED, submitSubject),
        takeEvery(FETCH_SUBJECT_REQUESTED, fetchSubject),
        takeEvery(DELETE_SUBJECT_REQUESTED, deleteSubject),

        takeEvery(FETCH_TEACHERS_REQUESTED, fetchTeachers),
        takeEvery(SUBMIT_TEACHER_REQUESTED, submitTeacher),
        takeEvery(FETCH_TEACHER_REQUESTED, fetchTeacher),
        takeEvery(DELETE_TEACHER_REQUESTED, deleteTeacher),
        
        takeEvery(FETCH_STUDENTS_REQUESTED, fetchStudents),
        takeEvery(SUBMIT_STUDENT_REQUESTED, submitStudent),
        takeEvery(FETCH_STUDENT_REQUESTED, fetchStudent),
        takeEvery(DELETE_STUDENT_REQUESTED, deleteStudent),
        
        takeEvery(FETCH_ASSIGNEDSUBJECTS_REQUESTED, fetchAssignedSubjects),
        takeEvery(SUBMIT_ASSIGNEDSUBJECT_REQUESTED, submitAssignedSubject),
        takeEvery(FETCH_ASSIGNEDSUBJECT_REQUESTED, fetchAssignedSubject),
        takeEvery(DELETE_ASSIGNEDSUBJECT_REQUESTED, deleteAssignedSubject),
        takeEvery(FETCH_COMBO_BOX_CAREERS_REQUESTED, fetchComboBoxCareers),
        takeEvery(FETCH_COMBO_BOX_YEARS_REQUESTED, fetchComboBoxYears),
        takeEvery(FETCH_COMBO_BOX_SUBJECTS_REQUESTED, fetchComboBoxSubjects),
        takeEvery(FETCH_COMBO_BOX_TEACHERS_REQUESTED, fetchComboBoxTeachers),
        takeEvery(FETCH_COMBO_BOX_STUDENTS_REQUESTED, fetchComboBoxStudents),

        takeEvery(FETCH_STUDENTNOTES_REQUESTED, fetchStudentNotes),
        takeEvery(SUBMIT_STUDENTNOTE_REQUESTED, submitStudentNote),
        takeEvery(FETCH_STUDENTNOTE_REQUESTED, fetchStudentNote),
        takeEvery(DELETE_STUDENTNOTE_REQUESTED, deleteStudentNote),
        takeEvery(FETCH_COMBO_SUBJECTS_REQUESTED, fetchComboSubjects),
        takeEvery(FETCH_COMBO_STUDENTS_REQUESTED, fetchComboStudents),
        takeEvery(FETCH_COMBO_TEACHERS_REQUESTED, fetchComboTeachers)
    ])

}