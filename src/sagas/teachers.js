import {call, put, select} from 'redux-saga/effects';
import TeacherService from '../services/teachers';
import {fetchTeachersSucceeded, submitTeacherSucceeded, 
              fetchTeacherSucceeded, deleteTeacherSucceeded} from '../actions/teachers';

export function* fetchTeachers({filter}) {
    const teachers = yield call(TeacherService.apiCall, filter)
    yield put(fetchTeachersSucceeded(teachers));
}

export function* submitTeacher() {
    const {currentTeachers} = yield select (state => state.teachers);
    const {status, data} = yield call(TeacherService.apiSave, currentTeachers);
    yield put(submitTeacherSucceeded(status, data));
  }

  export function * fetchTeacher ({id}){
    const teacher = yield call (TeacherService.apiCallOne, id)
    yield put(fetchTeacherSucceeded(teacher));
}

export function* deleteTeacher({id}){
  yield call(TeacherService.apiDelete,id);
  yield put(deleteTeacherSucceeded(true));
  yield call(fetchTeachers, {});
}