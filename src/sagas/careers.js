import {call, put, select} from 'redux-saga/effects';
import CareerService from '../services/careers';
import {fetchCareersSucceeded, submitCareerSucceeded, 
              fetchCareerSucceeded, deleteCareerSucceeded} from '../actions/careers';

export function* fetchCareers({filter}) {
    const careers = yield call(CareerService.apiCall, filter)
    yield put(fetchCareersSucceeded(careers));
}
export function* submitCareer() {
    const {currentCareers} = yield select (state => state.careers);
    const {status, data} = yield call(CareerService.apiSave, currentCareers);
    yield put(submitCareerSucceeded(status, data));
  }

  export function* fetchCareer({id}){
    const career = yield call(CareerService.apiCallOne, id)
    yield put(fetchCareerSucceeded(career))
  }

  export function* deleteCareer({id}){
    yield call(CareerService.apiDelete, id);
    yield put(deleteCareerSucceeded(true));
    yield call(fetchCareers, {});
  }