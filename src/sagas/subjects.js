import {call, put, select} from 'redux-saga/effects';
import SubjectService from '../services/subjects';
import {fetchSubjectsSucceeded, submitSubjectSucceeded, fetchSubjectSucceeded, 
            deleteSubjectSucceeded} from '../actions/subjects';



export function* fetchSubjects({filter}) {
    const subjects = yield call(SubjectService.apiCall, filter)
    yield put(fetchSubjectsSucceeded(subjects))
}

export function* submitSubject(){
    const {currentSubjects} = yield select (state => state.subjects);
    const {status, data} = yield call (SubjectService.apiSave, currentSubjects)
    yield put (submitSubjectSucceeded(status, data));
}

export function* fetchSubject({id}) {
    const subject = yield call (SubjectService.apiCallOne, id)
    yield put (fetchSubjectSucceeded(subject))
}

export function* deleteSubject({id}){
    yield call (SubjectService.apiDelete, id )
    yield put (deleteSubjectSucceeded(true))
    yield call (fetchSubjects, {})
}