import {call, put, select} from 'redux-saga/effects';
import StudentNoteService from '../services/studentNotes';
import {fetchStudentNotesSucceeded, fetchStudentNoteSucceeded, 
              submitStudentNoteSucceeded, deleteStudentNoteSucceeded, 
              fetchComboSubjectsSucceeded, fetchComboStudentsSucceeded,
              fetchComboTeachersSucceeded
            } from '../actions/studentNotes';

export function* fetchStudentNotes({filter}){
    const studentNotes = yield call (StudentNoteService.apiCall, filter)
    yield put (fetchStudentNotesSucceeded(studentNotes));
}

export function* fetchComboSubjects (){
  const subjects = yield call(StudentNoteService.apiCallSubjects);
 yield put(fetchComboSubjectsSucceeded(subjects))
}

export function* fetchComboStudents (){
  const students = yield call(StudentNoteService.apiCallStudents);
 yield put(fetchComboStudentsSucceeded(students))
}

export function* fetchComboTeachers (){
  const teachers = yield call(StudentNoteService.apiCallTeachers);
 yield put(fetchComboTeachersSucceeded(teachers))
}

export function* submitStudentNote() {
    const {currentStudentNotes} = yield select (state => state.studentNotes);
    const {status, data} = yield call(StudentNoteService.apiSave, currentStudentNotes);
    yield put(submitStudentNoteSucceeded(status, data));
  }
  
  export function * fetchStudentNote ({id}){
    const studentNote = yield call (StudentNoteService.apiCallOne, id)
    yield put(fetchStudentNoteSucceeded(studentNote));
}

export function* deleteStudentNote({id}){
  yield call(StudentNoteService.apiDelete,id);
  yield put(deleteStudentNoteSucceeded(true));
  yield call(fetchStudentNotes, {});
}
