import HTTP from './http';
const API = 'api/teachers';


export default class Teachers {
    static async apiCall() {
        return HTTP.get(API) 
    }

    static async apiCallOne(id) {
        return HTTP.get(`${API}/${id}`)
      }

      static async apiSave(teacher) {
        if (teacher.id){
          return HTTP.put(`${API}/${teacher.id}`,teacher)
        }else{        
          return HTTP.post(API, teacher)      
        }
      }

      static async apiDelete(id){
        return HTTP.delete(`${API}/${id}`)
      }
}
