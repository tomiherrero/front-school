import HTTP from './http';
const API = 'api/students';

export default class Students {
    static async apiCall() {
        return HTTP.get(API) 
    }

    static async apiCallOne(id) {
        return HTTP.get(`${API}/${id}`)
      }

      static async apiSave(student) {
        if (student.id){
          return HTTP.put(`${API}/${student.id}`,student)
        }else{        
          return HTTP.post(API, student)      
        }
      }
      
      static async apiDelete(id){
        return HTTP.delete(`${API}/${id}`)
      }
}