import HTTP from './http';
const API = 'api/subjects';


export default class Subjects {
    static async apiCall() {
        return HTTP.get(API)
    }
    static async apiCallOne(id){
        return HTTP.get(`${API}/${id}`)
    }
    static async apiSave (subject){
        if(subject.id){
            return HTTP.put(`${API}/${subject.id}`, subject )
        } else{
            return HTTP.post(API, subject)
        }
    }
    static async apiDelete(id){
        return HTTP.delete(`${API}/${id}`)
    }
}