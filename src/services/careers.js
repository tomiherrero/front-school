import HTTP from './http';
const API = 'api/careers';


export default class Careers {
    static async apiCall() {
        return HTTP.get(API) 
      }
      static async apiCallOne (id) {
        return HTTP.get(`${API}/${id}`)
   }
      static async apiSave(career) {
        if(career.id) {
           return HTTP.put(`${API}/${career.id}`, career)
        } else {
            return HTTP.post(API, career)
        }
      }
      static async apiDelete(id){
        return HTTP.delete(`${API}/${id}`)
      }

}