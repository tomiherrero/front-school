import HTTP from './http';
const API = 'api/studentNotes';

export default class StudentNotes{
    static async apiCall(){
        return HTTP.get(API)
    }
    
    static async apiCallSubjects(){
      return HTTP.get('api/subjects')
    }

    static async apiCallStudents(){
      return HTTP.get('api/students')
    }

    static async apiCallTeachers(){
      return HTTP.get('api/teachers')
    }

    static async apiCallOne(id) {
        return HTTP.get(`${API}/${id}`)
      }

      static async apiSave(studentNote) {
        if (studentNote.id){
          return HTTP.put(`${API}/${studentNote.id}`,studentNote)
        }else{        
          return HTTP.post(API, studentNote)      
        }
      }
      
      static async apiDelete(id){
        return HTTP.delete(`${API}/${id}`)
      }
}