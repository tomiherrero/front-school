import HTTP from './http';
const API = 'api/assignedSubjects';


export default class AssignedSubject {
    static async apiCall(){
        return HTTP.get(API);
    }

    static async apiCallCareers(){
        return HTTP.get('api/careers')
   }
   
   static async apiCallYears(){
       return HTTP.get('api/years')
   }
   
   static async apiCallSubjects(){
       return HTTP.get('api/subjects');
   }
   
   static async apiCallTeachers(){
       return HTTP.get('api/teachers')
   }
   
   static async apiCallStudents(){
       return HTTP.get('api/students')
   }

    static async apiCallOne(id){
        return HTTP.get(`${API}/${id}`)
    }

    static async apiSave(assignedSubject){
        if(assignedSubject.id){
            return HTTP.put(`${API}/${assignedSubject.id}`, assignedSubject)
        }else {
            return HTTP.post(API, assignedSubject)
        }
    }

    static async apiDelete(id){
        return HTTP.delete(`${API}/${id}`)
    }
}