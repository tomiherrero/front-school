import React, {Component} from 'react'; 
import {connect} from 'react-redux';
import {updateSubjects, submitSubjectRequested, fetchSubjectRequested} from '../../actions/subjects';
import {InputText} from '../../components/Form';
import { Form, Button, Container, Col, Row } from "reactstrap";


class Edit extends Component {

    componentDidMount(){
        const {id} = this.props.match.params;
        if(id){
            this.props.fetchSubject(id)
        }
    }
    handleChange(obj) {
        const {subject} = this.props;
        Object.assign(subject, obj);
        this.props.updateSubject(subject);
        this.forceUpdate();
    }
 render(){
     const {
         subject:{
             description,
             id
         }
     } = this.props;
     return (
        <Container>
        <Row>
          <Col className="breadcrumb float-left mt-5" sm={{ size: 5 }}>
            <Form>
              <h1>{id ? "Modificar" : "Agregar"}  Materia </h1>
              <hr />
              <InputText
                key="description"
                label="Materia : "
                value={description}
                placeholder="Escriba la materia"
                onChange={({ target: { value } }) =>
                  this.handleChange({ description: value })
                }
              />
            <Button
                outline
                color="info"
                size="lg"
                onClick={() => this.props.submit()}
              >
                {" "}
                Guardar
              </Button>
            </Form>
          </Col>
        </Row>
      </Container>
    );
}
}
 const mapStateToProps = state => ({
     subject: state.subjects.currentSubjects
 });
const mapDispatchToProps = dispatch => ({
    fetchSubject: id => dispatch(fetchSubjectRequested(id)),
    updateSubject: subject => dispatch(updateSubjects(subject)),
    submit: () => dispatch(submitSubjectRequested())

})

export default connect(mapStateToProps, mapDispatchToProps)(Edit);