import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import { Container, Row, Col, Button } from "reactstrap";
import Title from '../../components/Title';
import Table from '../../components/Table';
import {connect} from 'react-redux';
import {
  fetchSubjectsRequested, deleteSubjectRequested
} from '../../actions/subjects';

class Subjects extends Component {

    componentDidMount(){
        this.props.requestSubjects();
    }
    render() {
        const {headers, documents} = this.props;
        return (
            <Container>
            <Row>
              <Col sm="8" className="float-left mt-5">
                <Title title= "Materias"/>
              </Col>
              <Col sm="4" className="float-right mt-5">
                <Button
                  className="rounded-pill"
                  tag={Link}
                  color="btn btn-outline-dark"
                  block="Block level button"
                  to="/subjects/new"
                >
                 Agregar Materia
                </Button>
              </Col>
            </Row>
            <Row>
              <Col>
                <Table
                  {...{ documents, headers, linkTo: "/subjects" }}
                  onDelete={(id) => this.props.deletedSubject(id)}
                />
              </Col>
            </Row>
          </Container>
      );
      }
      }

const mapStateToProps = state => ({
    headers: state.subjects.headers,
    documents: state.subjects.subjects
});

const mapDispatchProps = dispatch => ({
    requestSubjects: () => dispatch(fetchSubjectsRequested()),
    deletedSubject: id => dispatch(deleteSubjectRequested(id))
})

export default connect(mapStateToProps, mapDispatchProps)(Subjects);