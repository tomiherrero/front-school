import React, {Component} from 'react'; 
import {connect} from 'react-redux';
import { Form, Button, Container, Col, Row } from "reactstrap";
import {updateCareers, submitCareerRequested, fetchCareerRequested} from '../../actions/careers'
import {InputText} from '../../components/Form';
class Edit extends Component {
    componentDidMount(){
        const {id} = this.props.match.params;
        if(id){
            this.props.fetchCareer(id)
        }
    }
        handleChange(obj){
        const {career} = this.props;
        Object.assign(career, obj);
        this.props.updateCareer(career);
        this.forceUpdate();
    }
    render() {
        const {
            career: {        
                description,
                id
            }
        } = this.props;
        return (
            <Container>
            <Row>
              <Col className="breadcrumb float-left mt-5" sm={{ size: 5 }}>
                <Form>
                  <h1>{id ? "Modificar" : "Agregar"}  Carrera </h1>
                  <hr />
                  <InputText
                    key="description"
                    label="Carrera : "
                    value={description}
                    placeholder="Escriba la carrera"
                    onChange={({ target: { value } }) =>
                      this.handleChange({ description: value })
                    }
                  />
                <Button
                    outline
                    color="info"
                    size="lg"
                    onClick={() => this.props.submit()}
                  >
                    {" "}
                    Guardar
                  </Button>
                </Form>
              </Col>
            </Row>
          </Container>
        );
  }
}
const mapStateToProps = state => ({
    career: state.careers.currentCareers
});
const mapDispatchToProps = dispatch => ({
    fetchCareer: id => dispatch(fetchCareerRequested(id)),
    updateCareer: career => dispatch (updateCareers(career)),
    submit: () => dispatch(submitCareerRequested())
});
export default connect( mapStateToProps, mapDispatchToProps)(Edit);