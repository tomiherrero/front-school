import React, {Component}from 'react';
import Title from '../../components/Title';
import Table from '../../components/Table';
import { Container, Row, Col, Button } from "reactstrap";
import {connect} from 'react-redux';
import {
  fetchCareersRequested, deleteCareerRequested,
} from '../../actions/careers'
import {Link} from 'react-router-dom'; 

class Careers extends Component {
 componentDidMount(){
    this.props.requestCareers();
  }
  render(){
    const {headers, documents} = this.props;
    return(
      <Container>
      <Row>
        <Col sm="8" className="float-left mt-5">
          <Title title="Carreras" />
        </Col>
        <Col sm="4" className="float-right mt-5">
          <Button
            className="rounded-pill"
            tag={Link}
            color="btn btn-outline-dark"
            block="Block level button"
            to="/careers/new"
          >
           Agregar carrera
          </Button>
        </Col>
      </Row>
      <Row>
        <Col>
          <Table
            {...{ documents, headers, linkTo: "/careers" }}
            onDelete={(id) => this.props.deletedCareer(id)}
          />
        </Col>
      </Row>
    </Container>
);
}
}

const mapStateToProps =  state  => ({
      headers: state.careers.headers,
      documents: state.careers.careers
});

const mapDispatchProps = dispatch => ({
  requestCareers: () => dispatch(fetchCareersRequested()),
  deletedCareer: id => dispatch(deleteCareerRequested(id))
})
export default connect(mapStateToProps, mapDispatchProps )(Careers);