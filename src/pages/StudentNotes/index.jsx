import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Edit from './Edit';
import List from './List';

const StudentNotes = ({match: {path}}) =>(
    <Switch>
    <Route path = {`${path}/new`} strict component = {Edit}/>  
    <Route path = {`${path}/:id`} strict component = {Edit}/>
    <Route path = {`${path}/`} component = {List}/>
    </Switch>
)

export default StudentNotes;