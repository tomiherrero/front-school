import React, {Component} from 'react'; 
import {connect} from 'react-redux';
import {updateStudentNotes, submitStudentNoteRequested, fetchStudentNoteRequested, 
              fetchComboSubjectsRequested, fetchComboStudentsRequested,
              fetchComboTeachersRequested
            } from '../../actions/studentNotes'
import {InputText, ComboBox} from '../../components/Form';
import { Form, Button, Container, Col, Row } from "reactstrap";


class Edit extends Component {
   
    componentDidMount(){

      this.props.fetchComboSubjects();
      this.props.fetchComboStudents();
      this.props.fetchComboTeachers();
        const {id} = this.props.match.params;
        if(id){
            this.props.fetchStudentNote(id)
        }
    }
        handleChange(obj){
        const {studentNote} = this.props;
        Object.assign(studentNote, obj);
        this.props.updateStudentNote(studentNote);
        this.forceUpdate();
    }
    render() {
      console.log(this.props.students)
            const {
            studentNote: {        
                note1,
                note2,
                note3,
                averages,
                subjects,
                students,
                teachers,
                id
            }
        } = this.props;
        return (
            <Container>
              <Row>
                <Col className="breadcrumb float-left mt-5" sm={{ size: 5 }}>
                  <Form>
                    <h1>{id ? "Modificar" : "Agregar"} Nota de estudiante </h1>
                    <hr />
                    <InputText
                      key="note1"
                      label="Nota 1: "
                      value={note1}
                      placeholder="Escriba Nota 1"
                      onChange={({ target: { value } }) =>
                        this.handleChange({note1: value })
                      }
                    />
                    <InputText
                      key="note2"
                      label="Nota 2: "
                      value={note2}
                      placeholder="Escriba Nota 2"
                      onChange={({ target: { value } }) =>
                        this.handleChange({note2: value })
                      }
                    />
                    <InputText
                      key="note3"
                      label="Nota 3: "
                      value={note3}
                      placeholder="Escriba Nota 3"
                      onChange={({ target: { value } }) =>
                        this.handleChange({note3: value })
                      }
                    />
                    <InputText
                      key="averages"
                      label="Promedio : "
                      value={averages}
                      placeholder="Escriba Promedio"
                      onChange={({ target: { value } }) =>
                        this.handleChange({averages: value })
                      }
                    />                
                    <ComboBox
                      key="subjects"
                      label = "Materia : "
                      value= {subjects}
                      onChange = {({target: {value}}) =>
                      this.handleChange({subjects: value})}
                      options= {this.props.subjects}
                      getOptionKey = {opt => opt.id}
                      getOptionLabel = {opt => opt.description }
                    />        
                    <ComboBox
                      key="students"
                      label = "Estudiante : "
                      value= {students}
                      onChange = {({target: {value}}) =>
                      this.handleChange({students: value})}
                      options= {this.props.students}
                      getOptionKey = {opt => opt.id}
                      getOptionLabel = {opt => `${opt.surname}, ${opt.name}` }                      
                    />        
                      <ComboBox
                      key="teachers"
                      label = "Profesor : "
                      value= {teachers}
                      onChange = {({target: {value}}) =>
                      this.handleChange({teachers: value})}
                      options= {this.props.teachers}
                      getOptionKey = {opt => opt.id}
                      getOptionLabel = {opt => `${opt.surname}, ${opt.name}` }           
                    />                   
                    <Button
                      outline
                      color="info"
                      size="lg"
                      onClick={() => this.props.submit()}
                    >
                      {" "}
                      Guardar
                    </Button>
                  </Form>
                </Col>
              </Row>
            </Container>
          );
    }
}
const mapStateToProps = state => ({
    studentNote: state.studentNotes.currentStudentNotes,
    subjects: state.studentNotes.subjects,
    students: state.studentNotes.students,
    teachers: state.studentNotes.teachers
});
const mapDispatchToProps = dispatch => ({
    fetchComboSubjects: () => dispatch(fetchComboSubjectsRequested()),
    fetchComboStudents: () => dispatch(fetchComboStudentsRequested()),
    fetchComboTeachers: () => dispatch(fetchComboTeachersRequested()),
    fetchStudentNote: id => dispatch(fetchStudentNoteRequested(id)),
    updateStudentNote: studentNote => dispatch (updateStudentNotes(studentNote)),
    submit: () => dispatch(submitStudentNoteRequested())
});
export default connect( mapStateToProps, mapDispatchToProps)(Edit);