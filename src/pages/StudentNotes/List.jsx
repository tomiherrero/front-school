import React, {Component}from 'react';
import Title from '../../components/Title';
import Table from '../../components/Table';
import {connect} from 'react-redux';
import { fetchStudentNotesRequested, deleteStudentNoteRequested} from '../../actions/studentNotes';
import {Link} from 'react-router-dom'; 
import { Container, Row, Col, Button } from "reactstrap";


class StudentNotes extends Component {
 componentDidMount(){
    this.props.requestStudentNotes();
  }
  render(){
    const {headers, documents} = this.props;
    return(
      <Container>
      <Row>
        <Col sm="8" className="float-left mt-5">
          <Title title="Notas de Estudiantes" />
        </Col>
        <Col sm="4" className="float-right mt-5">
          <Button
            className="rounded-pill"
            tag={Link}
            color="btn btn-outline-dark"
            block="Block level button"
            to="/studentNotes/new"
          >
           Agregar nota de estudiante
          </Button>
        </Col>
      </Row>
      <Row>
        <Col>
          <Table
            {...{ documents, headers, linkTo: "/studentNotes" }}
            onDelete={(id) => this.props.deletedStudentNote(id)}
          />
        </Col>
      </Row>
    </Container>
);
}
}
const mapStateToProps =  state  => ({
      headers: state.studentNotes.headers,
      documents: state.studentNotes.studentNotes
});

const mapDispatchProps = dispatch => ({
  requestStudentNotes: () => dispatch(fetchStudentNotesRequested()),
  deletedStudentNote: id => dispatch(deleteStudentNoteRequested(id))
})
export default connect(mapStateToProps, mapDispatchProps )(StudentNotes);