import React, {Component} from 'react'; 
import {connect} from 'react-redux';
import { Form, Button, Container, Col, Row } from "reactstrap";
import {updateStudents, submitStudentRequested, fetchStudentRequested} from '../../actions/students'
import {InputText, InputEmail} from '../../components/Form';

class Edit extends Component {
   
    componentDidMount(){
        const {id} = this.props.match.params;
        if(id){
            this.props.fetchStudent(id)
        }
    }
        handleChange(obj){
        const {student} = this.props;
        Object.assign(student, obj);
        this.props.updateStudent(student);
        this.forceUpdate();
    }
    render() {
        const {
            student: {        
                name,
                surname,
                phone,
                home,
                email,
                id
   
            }
        } = this.props;
        return (
            <Container>
            <Row>
              <Col className="breadcrumb float-left mt-5" sm={{ size: 5 }}>
                <Form>
                  <h1>{id ? "Modificar" : "Agregar"}  Estudiante</h1>
                  <hr />
                  <InputText
                    key="name"
                    label="Nombre: "
                    value={name}
                    placeholder="Escriba Nombre"
                    onChange={({ target: { value } }) =>
                      this.handleChange({ name: value })
                    }
                  />
                   <InputText
                    key="surname"
                    label="Apellido: "
                    value={surname}
                    placeholder="Escriba Apellido"
                    onChange={({ target: { value } }) =>
                      this.handleChange({ surname: value })
                    }
                  />
                   <InputText
                    key="phone"
                    label="Telefono: "
                    value={phone}
                    placeholder="Escriba Telefono"
                    onChange={({ target: { value } }) =>
                      this.handleChange({ phone: value })
                    }
                  />
                   <InputText
                    key="home"
                    label="Direccion: "
                    value={home}
                    placeholder="Escriba Direccion"
                    onChange={({ target: { value } }) =>
                      this.handleChange({ home: value })
                    }
                  />
                   <InputEmail
               key="email"
                label="Email: "
                value={email}
                placeholder="Escriba email"
                onChange={({ target: { value } }) =>
                  this.handleChange({ email: value })
                }
              />
                  <Button
                    outline
                    color="info"
                    size="lg"
                    onClick={() => this.props.submit()}
                  >
                    {" "}
                    Guardar
                  </Button>
                </Form>
              </Col>
            </Row>
          </Container>
        )
    }
}
const mapStateToProps = state => ({
    student: state.students.currentStudents
});
const mapDispatchToProps = dispatch => ({
    fetchStudent: id => dispatch(fetchStudentRequested(id)),
    updateStudent: student => dispatch (updateStudents(student)),
    submit: () => dispatch(submitStudentRequested())
});
export default connect( mapStateToProps, mapDispatchToProps)(Edit);