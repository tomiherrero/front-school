import React, {Component}from 'react';
import Title from '../../components/Title';
import Table from '../../components/Table';
import { Container, Row, Col, Button } from "reactstrap";
import {connect} from 'react-redux';
import {fetchStudentsRequested, deleteStudentRequested} from '../../actions/students'
import {Link} from 'react-router-dom'; 

class Students extends Component {
 componentDidMount(){
    this.props.requestStudents();
  }
  render(){
    const {headers, documents} = this.props;
    return(
      <Container>
      <Row>
        <Col sm="8" className="float-left mt-5">
          <Title title="Estudiantes" />
        </Col>
        <Col sm="4" className="float-right mt-5">
          <Button
            className="rounded-pill"
            tag={Link}
            color="btn btn-outline-dark"
            block="Block level button"
            to="/students/new"
          >
           Agregar Estudiante
          </Button>
        </Col>
      </Row>
      <Row>
        <Col>
          <Table
            {...{ documents, headers, linkTo: "/students" }}
            onDelete={(id) => this.props.deletedStudent(id)}
          />
        </Col>
      </Row>
    </Container>
    );
    }
  }

const mapStateToProps =  state  => ({
      headers: state.students.headers,
      documents: state.students.students
});

const mapDispatchProps = dispatch => ({
  requestStudents: () => dispatch(fetchStudentsRequested()),
  deletedStudent: id => dispatch(deleteStudentRequested(id))
})
export default connect(mapStateToProps, mapDispatchProps )(Students);
