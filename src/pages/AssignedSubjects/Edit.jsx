import React, { Component } from "react";
import { connect } from "react-redux";
import { Form, Button, Container, Col, Row } from "reactstrap";
import { ComboBox } from "../../components/Form";
import {  updateAssignedSubjects, 
                submitAssignedSubjectRequested, 
                fetchAssignedSubjectRequested, 
                fetchComboBoxCareersRequested, 
                fetchComboBoxYearsRequested, 
                fetchComboBoxSubjectsRequested, 
                fetchComboBoxTeachersRequested, 
                fetchComboBoxStudentsRequested
} from '../../actions/assignedSubjects';


class Edit extends Component {
    componentDidMount(){
        this.props.fetchComboBoxCareers();
        this.props.fetchComboBoxYears();
        this.props.fetchComboBoxSubjects();
        this.props.fetchComboBoxTeachers();
        this.props.fetchComboBoxStudents();
        const {id} = this.props.match.params;
        if(id) {
            this.props.fetchAssignedSubject(id);  
        }
    }
        handleChange(obj){
        const {assignedSubject} = this.props;
        Object.assign(assignedSubject, obj);
        this.props.updateAssignedSubject(assignedSubject);
        this.forceUpdate();
    }
    render() {
        const {
            assignedSubject: {     
                careers,  
                years,
                subjects, 
                teachers,
                students,
                id
            }
        } = this.props;
        return (
          
            <Container>
              <Row>
                <Col className="breadcrumb float-left mt-5" sm={{ size: 5 }}>
                  <Form>
                        <h1>{id ? "Modificar" : "Agregar"}  Materia asignada</h1>
                        <hr />
                        <ComboBox
                            key="careers"
                            label = "Carrera : "
                            value= {careers}
                            onChange = {({target: {value}}) =>
                            this.handleChange({careers: value})}
                            options= {this.props.careers}
                            getOptionKey={opt => opt.id}
                            getOptionLabel = {opt => opt.description}     
                        />

                        <ComboBox
                            key="years"
                            label= "Año :"
                            value = {years}
                            onChange= {({target: {value}}) =>
                            this.handleChange({years: value})}
                            options ={this.props.years}
                            getOptionKey = {opt => opt.id}
                            getOptionLabel = {opt => opt.description} 
                        />

                        <ComboBox
                            key= "subjects"
                            label="Materias : "
                            value= {subjects}
                            onChange ={({target: {value}}) =>
                            this.handleChange({subjects: value})}
                            options = {this.props.subjects}
                            getOptionKey={opt => opt.id}
                            getOptionLabel= {opt => opt.description}
                        />

                        <ComboBox 
                            key="teachers"
                            label="Profesores: "
                            value= {teachers}
                            onChange ={({target: {value}}) =>
                            this.handleChange({teachers: value})}
                            options = {this.props.teachers}
                            getOptionKey={opt => opt.id}
                            getOptionLabel= {opt => `${opt.surname}, ${opt.name}`}
                        />

                        <ComboBox
                            key="students"
                            label="Alumnos: "
                            value= {students}
                            onChange = {({target: {value}}) =>
                            this.handleChange({students: value})}
                            options= {this.props.students}
                            getOptionKey= {opt => opt.id}
                            getOptionLabel= {opt => `${opt.surname}, ${opt.name}`}
                        />

                        <Button
                            outline
                            color="info"
                            size="lg"
                            onClick={() => this.props.submit()}>
                                    {" "}
                                    Guardar
                        </Button>
                  </Form>
                </Col>
              </Row>
            </Container>
        );
    }
}

const mapStateToProps = state => ({
    assignedSubject: state.assignedSubjects.currentAssignedSubjects,
    careers: state.assignedSubjects.careers,
    years: state.assignedSubjects.years,
    subjects: state.assignedSubjects.subjects,
    teachers: state.assignedSubjects.teachers,
    students: state.assignedSubjects.students
});

const mapDispatchToProps = dispatch => ({
    submit: () => dispatch(submitAssignedSubjectRequested()),
    fetchComboBoxStudents: () => dispatch(fetchComboBoxStudentsRequested()),
    fetchComboBoxTeachers: () => dispatch(fetchComboBoxTeachersRequested()), 
    fetchComboBoxSubjects: () => dispatch(fetchComboBoxSubjectsRequested()),
    fetchComboBoxYears: () => dispatch(fetchComboBoxYearsRequested()),
    fetchComboBoxCareers: () => dispatch(fetchComboBoxCareersRequested()),
    fetchAssignedSubject: id => dispatch(fetchAssignedSubjectRequested(id)),
    updateAssignedSubject: assignedSubject => dispatch (updateAssignedSubjects(assignedSubject))
    
});
export default connect( mapStateToProps, mapDispatchToProps)(Edit);