import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Container, Row, Col, Button } from "reactstrap";
import Title from '../../components/Title';
import Table from '../../components/Table';
import {fetchAssignedSubjectsRequested, deleteAssignedSubjectRequested} from '../../actions/assignedSubjects';

class AssignedSubjects extends Component {
    componentDidMount() {
        this.props.requestAssignedSubjects();
    }
    render(){
        const{headers, documents} = this.props;
        return(
                <Container>
                  <Row>
                    <Col sm="8" className="float-left mt-5">
                      <Title title="Materias Asignadas" />
                    </Col>
                    <Col sm="4" className="float-right mt-5">
                      <Button
                        className="rounded-pill"
                        tag={Link}
                        color="btn btn-outline-dark"
                        block="Block level button"
                        to="/assignedSubjects/new"
                      >
                       Agregar materias asignadas
                      </Button>
                    </Col>
                  </Row>
                  <Row>
                    <Col>
                      <Table
                        {...{ documents, headers, linkTo: "/assignedSubjects" }}
                        onDelete={(id) => this.props.deletedAssignedSubjects(id)}
                      />
                    </Col>
                  </Row>
                </Container>
        );
    }
}

const mapStateToProps = state => ({
    headers: state.assignedSubjects.headers,
    documents: state.assignedSubjects.assignedSubjects
});

const mapDispatchProps = dispatch => ({
    requestAssignedSubjects: () => dispatch(fetchAssignedSubjectsRequested()),
    deletedAssignedSubjects: id => dispatch(deleteAssignedSubjectRequested(id))
});

export default connect(mapStateToProps, mapDispatchProps)(AssignedSubjects);