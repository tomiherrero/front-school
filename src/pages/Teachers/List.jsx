import React, {Component}from 'react';
import Title from '../../components/Title';
import Table from '../../components/Table';
import { Container, Row, Col, Button } from "reactstrap";
import {connect} from 'react-redux';
import { fetchTeachersRequested, deleteTeacherRequested} from '../../actions/teachers';
import {Link} from 'react-router-dom'; 


class Teachers extends Component {
 componentDidMount(){
    this.props.requestTeachers();
  }
  render(){
    const {headers, documents} = this.props;
    return(
      <Container>
      <Row>
        <Col sm="8" className="float-left mt-5">
          <Title title="Profesores" />
        </Col>
        <Col sm="4" className="float-right mt-5">
          <Button
            className="rounded-pill"
            tag={Link}
            color="btn btn-outline-dark"
            block="Block level button"
            to="/teachers/new"
          >
           Agregar Profesor
          </Button>
        </Col>
      </Row>
      <Row>
        <Col>
          <Table
            {...{ documents, headers, linkTo: "/teachers" }}
            onDelete={(id) => this.props.deletedTeacher(id)}
          />
        </Col>
      </Row>
    </Container>
    );
    }
  }

const mapStateToProps =  state  => ({
      headers: state.teachers.headers,
      documents: state.teachers.teachers
});

const mapDispatchProps = dispatch => ({
  requestTeachers: () => dispatch(fetchTeachersRequested()),
  deletedTeacher: id => dispatch(deleteTeacherRequested(id))
})
export default connect(mapStateToProps, mapDispatchProps )(Teachers);
