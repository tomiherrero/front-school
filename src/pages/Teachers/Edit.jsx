import React, {Component} from 'react'; 
import {connect} from 'react-redux';
import { Form, Button, Container, Col, Row } from "reactstrap";
import {updateTeachers, submitTeacherRequested, fetchTeacherRequested} from '../../actions/teachers'
import {InputText, InputEmail} from '../../components/Form';

class Edit extends Component {

    componentDidMount(){
        const {id} = this.props.match.params;
        if(id){
            this.props.fetchTeacher(id)
        }
    }
        handleChange(obj){
        const {teacher} = this.props;
        Object.assign(teacher, obj);
        this.props.updateTeacher(teacher);
        this.forceUpdate();
    }
    render() {
        const {
            teacher: {        
                name,
                surname,
                phone,
                home,
                email,
                id
   
            }
        } = this.props;
        return (
            <Container>
            <Row>
              <Col className="breadcrumb float-left mt-5" sm={{ size: 5 }}>
                <Form>
                  <h1>{id ? "Modificar" : "Agregar"}  Profesor</h1>
                  <hr />         
                  <InputText
                    key="name"
                    label="Nombre: "
                    value={name}
                    placeholder="Escriba Nombre"
                    onChange={({ target: { value } }) =>
                      this.handleChange({ name: value })
                    }
                  />
                   <InputText
                    key="surname"
                    label="Apellido: "
                    value={surname}
                    placeholder="Escriba Apellido"
                    onChange={({ target: { value } }) =>
                      this.handleChange({ surname: value })
                    }
                  />
                   <InputText
                    key="phone"
                    label="Telefono: "
                    value={phone}
                    placeholder="Escriba Telefono"
                    onChange={({ target: { value } }) =>
                      this.handleChange({ phone: value })
                    }
                  />
                   <InputText
                    key="home"
                    label="Direccion: "
                    value={home}
                    placeholder="Escriba Direccion"
                    onChange={({ target: { value } }) =>
                      this.handleChange({ home: value })
                    }
                  />
                   <InputEmail
                key="email"
                label="Email: "
                value={email}
                placeholder="Escriba e    id: nullmail"
                onChange={({ target: { value } }) =>
                  this.handleChange({ email: value })
                  }
                  />
                  <Button
                    outline
                    color="info"
                    size="lg"
                    onClick={() => this.props.submit()}
                  >
                    {" "}
                    Guardar
                  </Button>
                </Form>
              </Col>
            </Row>
          </Container>
        )
    }
}
const mapStateToProps = state => ({
    teacher: state.teachers.currentTeachers
});

const mapDispatchToProps = dispatch => ({
    fetchTeacher: id => dispatch(fetchTeacherRequested(id)),
    updateTeacher: teacher => dispatch (updateTeachers(teacher)),
    submit: () => dispatch(submitTeacherRequested())
});

export default connect( mapStateToProps, mapDispatchToProps)(Edit);