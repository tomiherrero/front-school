import React from "react";

const Title = ({ title }) => <h1 className="breadcrumb"> {title} </h1>;

export default Title;
