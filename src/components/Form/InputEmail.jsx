import React from "react";
import { FormGroup, Label, Input } from "reactstrap";

const InputEmail = ({ ...props }) => (
  <FormGroup>
    {props.label && <Label>{props.label}</Label>}
    <Input type="email" {...props} />
  </FormGroup>
);

export default InputEmail;
