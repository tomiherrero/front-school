import React from 'react';
import PropTypes from 'prop-types';
import {FormGroup, Input, Label} from 'reactstrap'
import map from 'lodash/map'

const ComboBox =({options, onChange,label, value, getOptionLabel, getOptionKey}) => (
    <FormGroup>
            {label && <Label>{label}</Label>}
            <Input type="select"   value={value} onChange={onChange}>
                {map(options, opt =>(
                    <option value={getOptionKey(opt)}>{getOptionLabel(opt)} </option>
                ))}
            </Input>
    </FormGroup>
)

ComboBox.propTypes = {
    options: PropTypes.arrayOf(PropTypes.shape({
        _id: PropTypes.string,
        name: PropTypes.string
    })),
    getOptionLabel: PropTypes.func, 
    onChange: PropTypes.func.isRequired,
    value: PropTypes.string.isRequired
};

ComboBox.defaultProps = {
    options: [],
    getOptionKey: opt => opt.id,
    getOptionLabel: opt => opt.description
}

export default ComboBox;